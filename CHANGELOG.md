# URL Shortener change log

## 0.0.1-beta.1 (August 04, 2022)

* Added core functionalities for the app
  - Shortening long links
  - Retrieving long links from short links
  - Saving data to database
* Added database schemas
* Creation of url shortener back end