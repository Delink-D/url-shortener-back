import { Module } from '@nestjs/common';
import { AuthModule, ApiKeyGuardModule, UrlModule, UserModule, PrismaModule } from './app';

@Module({
  imports: [AuthModule, UrlModule, UserModule, PrismaModule, ApiKeyGuardModule],
  controllers: [],
  providers: [],
})
export class AppModule { }
