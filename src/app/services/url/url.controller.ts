import { Body, Controller, Get, HttpException, Param, Post, Res, UseGuards } from '@nestjs/common';
import { Response } from 'express';
import { ApiKeyGuard, UrlDto } from '../../core';
import { UrlService } from './url.service';

@Controller()
export class UrlController {
  constructor(private urlService: UrlService) { }

  @Post('shorten')
  @UseGuards(ApiKeyGuard)
  async shortenUrl(@Body() dto: UrlDto) {
    try {
      // call service to shorten link
      const result = await this.urlService.shortenUrl(dto.link);

      return result;
    } catch (error) {
      throw new HttpException(error.message, error.statusCode);
    }
  }

  @Get(':slug')
  async getLongLink(@Param('slug') slug: string, @Res() res: Response) {
    try {
      res.redirect((await this.urlService.getLongUrl(slug)).longLink);
    } catch (error) {
      throw error;
    }
  }
}
