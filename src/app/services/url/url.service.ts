import { Injectable } from '@nestjs/common';
import * as NanoId from 'nanoid/async';
import { PrismaService } from '../../core';

// set the characters to use and default to 9 characters
const nanoid = NanoId.customAlphabet(process.env.CUSTOM_ALPHABET, 10);

@Injectable()
export class UrlService {
  constructor(private prisma: PrismaService) { }

  /**
   * Function shorten long url links
   * @param url long url to shorten
   * @returns return an object of shortened link
   */
  async shortenUrl(url: string) {
    try {
      // find if url exist before saving
      const data = await this.prisma.url.findFirst({
        where: {
          longLink: url,
        },
      });

      // link already shortened
      if (data) {
        return this.returnLinkObject(data);
      }

      // get short link code
      const shortId: string = await nanoid(7);

      // short link
      const shortUrl = `${process.env.SHORT_DOMAIN_URL}${shortId}`;

      // save shortened link
      const saved = await this.prisma.url.create({
        data: {
          longLink: url,
          shortLink: shortUrl,
          slug: shortId,
        },
      });

      return this.returnLinkObject(saved);

    } catch (error) {
      throw this.prisma.prismaErrorHandler(error);
    }
  }

  /**
   * Only return long link and short link back to client
   * @param record full url record object
   */
  returnLinkObject(record: any) {
    return { message: 'URL shortened successfully.', data: { longLink: record.longLink, shortLink: record.shortLink } };
  }

  /**
   * Function get the long url from slug string
   * @param slug url slug string
   */
  async getLongUrl(slug: string) {
    try {
      // find the first slug or else if no slug found throw an error
      const data = await this.prisma.url.findFirstOrThrow({
        where: {
          slug: {
            equals: slug,
          },
        },
      });

      return data; // return data to the controller
    } catch (error) {
      throw this.prisma.prismaErrorHandler(error);
    }
  }
}
