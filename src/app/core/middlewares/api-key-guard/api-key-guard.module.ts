import { Global, Module } from '@nestjs/common';
import { ApiKeyGuard } from './api-key-guard.service';

@Global()
@Module({
  providers: [ApiKeyGuard],
  exports: [ApiKeyGuard],
})
export class ApiKeyGuardModule { }
