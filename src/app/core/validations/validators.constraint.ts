import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

@ValidatorConstraint({ name: 'isShortLink', async: true })
@Injectable()
export class IsShortLinkConstraint implements ValidatorConstraintInterface {
  constructor() { }

  async validate(link: string) {
    const regex = /^(https?:\/\/)?(www\.)?dlnk\.com\/?$/i;
    return !regex.test(link);
  }

  defaultMessage() {
    return 'Please try another link, this is already a shortened link.';
  }
}
