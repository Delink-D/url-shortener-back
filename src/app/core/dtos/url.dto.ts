import { IsNotEmpty, IsUrl, Validate } from 'class-validator';
import { IsShortLinkConstraint } from '../validations';

export class UrlDto {
  @IsUrl()
  @IsNotEmpty()
  @Validate(IsShortLinkConstraint)
  link: string;
}
