import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { NotFoundError } from '@prisma/client/runtime';

@Injectable()
export class PrismaService extends PrismaClient {
  constructor() {
    super({
      datasources: {
        db: {
          url: process.env.DATABASE_URL,
        },
      },
    });
  }

  /**
   * function filter prisma error
   * @param error prisma error to filter
   * @returns exceptional error
   */
  prismaErrorHandler(error: any) {
    if (error instanceof NotFoundError) {
      return new NotFoundException();
    } else {
      return error;
    }
  }
}
